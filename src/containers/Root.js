import React from 'react'
import ProductList from '../containers/ProductList'
import { Provider } from 'mobx-react'
import ProductStore from '../stores/ProductStore'
import DevTools from 'mobx-react-devtools'

const Root = () => (
  <div>
    <Provider ProductStore={ ProductStore }>
      <ProductList />
    </Provider>
    <DevTools></DevTools>
  </div>
)

export default Root
