import React from 'react'
import { inject, observer } from 'mobx-react'

import Product from '../components/Product'
import ProductStore from '../stores/ProductStore'

@inject('ProductStore')
@observer
class ProductList extends React.Component {

  handleBuyClick = id => {
    this.props.ProductStore.buyProduct(id)
  }

  handleClick = name => {
    this.props.ProductStore.sortProducts(name)
  }
  render() {
    const { products, soldProductsNumber } = this.props.ProductStore

    return (
        <ul className="products-list" >
          <button onClick={this.handleClick}>
            Sort it!
          </button>
          { ProductStore.products.map(p => (
            <li key={ p.id }>
              <Product
                id={ p.id }
                name={ p.name }
                tag={ p.tag }
                isSold={ p.isSold }
                onBuyClick={ this.handleBuyClick }
              />
            </li>
          ) ) }
        </ul>
    )
  }
}

export default ProductList
