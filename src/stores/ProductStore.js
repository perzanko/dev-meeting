import { action, computed, observable } from 'mobx'

class ProductStore {
  //4/ We have to use @observable to let MobX track changes
  @observable products = [
    { id: 0, name: 'Apple', tag: '200 PLN', isSold: false,},
    { id: 1, name: 'Banana', tag: '200 PLN', isSold: false,},
    { id: 2, name: 'Orange', tag: '200 PLN', isSold: false,},
    { id: 3, name: 'Pineapple', tag: '200 PLN', isSold: false,},
    { id: 4, name: 'Mango', tag: '200 PLN', isSold: false,},
    { id: 5, name: 'Kiwi', tag: '200 PLN', isSold: false,},
    { id: 6, name: 'Apricot', tag: '200 PLN', isSold: false,},
    { id: 7, name: 'Lemon', tag: '200 PLN', isSold: false,},
    { id: 8, name: 'Cherry', tag: '200 PLN', isSold: false,},
  ]

  @action buyProduct = id => {
    const productToBeSold = this.products.find(p => p.id === id)
    productToBeSold.isSold = true
  }

  @computed get soldProductsNumber () {
    return this.products.filter(p => p.isSold).length
  }

  @action sortProducts = name => {
    const arr = this.products;
    arr.sort(compare);
    function compare(a,b) {
      if (a.name < b.name)
        return -1;
      if (a.name > b.name)
        return 1;
      return 0;
    }
    this.products = arr.sort(compare);
  }
}

// We have to create an instance of the store
export default new ProductStore()
