import React from 'react'
import { observer } from 'mobx-react'


const Product = ({ id, name, tag, isSold, onBuyClick }) => {
  const handleClick = () => {
    onBuyClick(id)
  }

  return (
    <div>
      <span style={ { textDecoration: isSold ? 'line-through' : 'none' }}>
        { name }
        <h3>{ tag }</h3>
      </span>
      <button onClick={ handleClick }>
        Buy
      </button>
    </div>
  )
}

export default observer(Product)
